package globalTests;

import globalPageObjects.AuthorPage;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import basePageObjects.LoginPage;
import basicTestObjects.BasicTestObjects;

/**
Author: dparakrama
Created : Feb 28, 2018
 */
public class AuthorTests extends BasicTestObjects {

	@Parameters({"GlobalURL", "UserName", "Password"})
	@Test
    public void verifyDisiplines (String URL, String UserName, String Password) throws InterruptedException {
		launchBrowser(URL);
		LoginPage LoginPage = new LoginPage(driver);
		LoginPage.loginToTheApplication(UserName, Password);
	
		AuthorPage AuthorPage = new AuthorPage(driver);
		AuthorPage.navigateToAuthor();
		AuthorPage.verifyDisiplinesReading();
		AuthorPage.verifyDisiplinesWriting();
		AuthorPage.verifyDisiplinesNumeracy();
		AuthorPage.verifyConventionsofLanguage();
	}
	
	
	
}

