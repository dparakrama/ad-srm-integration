package globalTests;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import basePageObjects.LoginPage;
import basicTestObjects.BasicTestObjects;

import globalPageObjects.ManageDeliveryPage;
/**
Author: forbeschenh
Created : Feb 28, 2018
 */
public class ManageDeliveryTest extends BasicTestObjects{
	
	@Parameters({"GlobalURL", "UserName", "Password"})
	@Test
    public void verifyAssessmentEvents (String URL, String UserName, String Password) throws InterruptedException {
		launchBrowser(URL);
		LoginPage LoginPage = new LoginPage(driver);
		LoginPage.loginToTheApplication(UserName, Password);
	
		ManageDeliveryPage ManageDeliveryPage = new ManageDeliveryPage(driver);
		ManageDeliveryPage.navigateManageDelivery();
		ManageDeliveryPage.createWritingSBAE();
		ManageDeliveryPage.createNumeracySBAE();
		ManageDeliveryPage.createReadingAndConventionsSBAE();
		
		
		
	}
	
	
	
}
