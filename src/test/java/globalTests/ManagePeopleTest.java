package globalTests;

import globalPageObjects.ManagePeoplePage;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import basePageObjects.LoginPage;
import basicTestObjects.BasicTestObjects;

/**
Author: dparakrama
Created : Feb 28, 2018
 */
public class ManagePeopleTest extends BasicTestObjects {
	
	@Parameters({"GlobalURL", "UserName", "Password"})
	@Test
    public void verifyDisabilityAdjustment (String URL, String UserName, String Password) throws InterruptedException {
		launchBrowser(URL);
		LoginPage LoginPage = new LoginPage(driver);
		LoginPage.loginToTheApplication(UserName, Password);
		ManagePeoplePage ManagePeoplePage = new ManagePeoplePage(driver);
		ManagePeoplePage.navigateToManagePeople();
		ManagePeoplePage.verifyDisabilityAdjustmentCodes();
}

}