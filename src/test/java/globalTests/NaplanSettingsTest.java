package globalTests;

import globalPageObjects.NaplanSettingsPage;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import basePageObjects.LoginPage;
import basicTestObjects.BasicTestObjects;

/**
Author: dparakrama
Created : Feb 28, 2018
 */
public class NaplanSettingsTest extends BasicTestObjects {
	
	@Parameters({"GlobalURL", "UserName", "Password"})
	@Test
    public void verifyNaplanSettings (String URL, String UserName, String Password) throws InterruptedException {
		launchBrowser(URL);
		LoginPage LoginPage = new LoginPage(driver);
		LoginPage.loginToTheApplication(UserName, Password);
		NaplanSettingsPage NaplanSettingsPage = new NaplanSettingsPage(driver);
		NaplanSettingsPage.navigateToNaplanSettings();
		NaplanSettingsPage.verifySRMConnection();
}

}