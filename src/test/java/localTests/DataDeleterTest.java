package localTests;

import globalPageObjects.ManageDeliveryPage;
import localPageObjects.ManagePeoplePage;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import basePageObjects.LoginPage;
import basicTestObjects.BasicTestObjects;

/**
Author: dparakrama
Created : Mar 15, 2018
 */
public class DataDeleterTest extends BasicTestObjects {

	@Parameters({"LocalURL", "UserName", "Password"})
	@Test
    public void deleteLocalData(String URL, String UserName, String Password) throws InterruptedException {
		launchBrowser(URL);
		LoginPage LoginPage = new LoginPage(driver);
		LoginPage.loginToTheApplication(UserName, Password);
		ManagePeoplePage ManagePeoplePage = new ManagePeoplePage(driver);
		ManagePeoplePage.DeleteData();
	}
	
	@Parameters({"GlobalURL", "UserName", "Password"})
	@Test
    public void deleteGlobalData(String URL, String UserName, String Password) throws InterruptedException {
		launchBrowser(URL);
		LoginPage LoginPage = new LoginPage(driver);
		LoginPage.loginToTheApplication(UserName, Password);
		ManageDeliveryPage ManageDeliveryPage = new ManageDeliveryPage(driver);
		ManageDeliveryPage.DeleteAssessmentEvents();
	}
	
}

