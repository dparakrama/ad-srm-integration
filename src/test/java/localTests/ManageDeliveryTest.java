package localTests;



import localPageObjects.ManageDeliveryPage;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import basePageObjects.LoginPage;
import basicTestObjects.BasicTestObjects;

/**
Author: dparakrama
Created : Mar 14, 2018
 */
public class ManageDeliveryTest extends BasicTestObjects{
	@Parameters({"LocalURL", "UserName", "Password"})
	@Test
    public void setYearLevel (String URL, String UserName, String Password) throws InterruptedException {
		launchBrowser(URL);
		LoginPage LoginPage = new LoginPage(driver);
		LoginPage.loginToTheApplication(UserName, Password);
		ManageDeliveryPage ManageDeliveryPage = new ManageDeliveryPage(driver);

		ManageDeliveryPage.setWritingEvent();
		ManageDeliveryPage.setNumeracyEvent();
		ManageDeliveryPage.setRCOLEventEvent();
	
}
}

