package localTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import basePageObjects.LoginPage;
import basicTestObjects.BasicTestObjects;

/**
 * Author: dparakrama Created : Mar 14, 2018
 */
public class IntegrationsTest extends BasicTestObjects {
	@Parameters({ "LocalURL", "UserName", "Password" })
	@Test
	public void setYearLevel(String URL, String UserName, String Password)
			throws InterruptedException {
		launchBrowser(URL);
		LoginPage LoginPage = new LoginPage(driver);
		LoginPage.loginToTheApplication(UserName, Password);
		driver.get("https://esa.adssit.ads.janisoncloud.com/naplan/srmintegration/index?menu=Integration");
		//This is needed for the loop to work because it is finding 'SRM-AUTOMATED-JURISDICTION-DNT' text in the table.
		//It navigates to go to the last page of the table
		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id=\"SRMBatchGrid\"]/div/a[4]/span")).click(); 
		Thread.sleep(15000);
		driver.findElement(By.xpath("//*[@id='content']/div[5]/div/h2"))
				.click();
		Thread.sleep(4000);
		
		/*
		 * List pagination = driver.findElements(By
		 * .xpath(".//*[@class='k-pager-wrap k-grid-pager k-widget']//a")); int
		 * size = pagination.size();
		 */
		
		for (int i = 0; i < 100; i++) {

			int isPresent = driver
					.findElements(
							By.xpath("//*[contains(text(), 'SRM-AUTOMATED-JURISDICTION-DNT')]"))
					.size();
			
			if (isPresent > 0) {
				WebElement element = driver
						.findElement(By
								.xpath("//*[contains(text(), 'SRM-AUTOMATED-JURISDICTION-DNT')]"));
				Actions builder = new Actions(driver);
				builder.moveToElement(element, +1000, +1).click().build()
						.perform();
				Thread.sleep(5000);
				builder.moveToElement(element, +568, +96).click().build()
						.perform();
				Thread.sleep(5000);
				driver.findElement(
						By.xpath(".//*[@id='select2-drop']/ul/li[2]/div"))
						.click();
				Thread.sleep(5000);
				builder.moveToElement(element, +1, +206).click().build()
						.perform();
				Thread.sleep(5000);
				break;
			}

			else {
				
				driver.findElement(
						By.xpath("//*[@id='content']/div[5]/div/section/div/div[3]/div/div[2]/a[3]/span"))
						.click();
				
				
			}

		}
	}

}