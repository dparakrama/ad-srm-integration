package basicTestObjects;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import com.relevantcodes.extentreports.LogStatus;

public class BasicTestObjects extends ExtentManager {
	protected WebDriver driver;

	/***
	 * Test Method for invoking the browser
	 * 
	 * @throws InterruptedException
	 * **/

	public void launchBrowser(String Url) throws InterruptedException {
		ChromeDriverManager.getInstance().setup();
		getInstance();
		test = report.startTest((this.getClass().getSimpleName()));
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(Url);
		test.log(LogStatus.PASS, "Browser Session Strated");
		Thread.sleep(5000);

	}

	/***
	 * Test Methods Close the Browser
	 * 
	 * **/
	@AfterMethod
	public void closeSession() {
		report.endTest(test);
		report.flush();
		driver.quit();
		test.log(LogStatus.PASS, "Browser Session Ended");
	}

}