package srmTests;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import srmPageObjects.CreateGroupPage;
import basePageObjects.LoginPage;
import basicTestObjects.BasicTestObjects;

/**
Author: dparakrama
Created : Mar 1, 2018
 */
public class CreateGroupTest extends BasicTestObjects{
	@Parameters({"SRMURL", "UserName", "Password"})
	@Test
    public void verifySRMFolderCreation (String URL, String UserName, String Password) throws InterruptedException {
		launchBrowser(URL);
		LoginPage LoginPage = new LoginPage(driver);
		LoginPage.loginToTheApplication(UserName, Password);
		CreateGroupPage CreateGroupPage = new CreateGroupPage(driver);
		CreateGroupPage.createSRMFolderScructure();		
		CreateGroupPage.createAStudent();
}

}