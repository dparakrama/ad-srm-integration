package srmTests;

import java.io.IOException;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import basePageObjects.LoginPage;
import basicTestObjects.BasicTestObjects;
import srmPageObjects.SRMCheckList;

/**
Author: fchenh
Created : 24 Jan. 2019
*/
public class SRMCheckListTest extends BasicTestObjects {
	@Parameters({"SRMURL", "UserName", "Password"})
	@Test
    public void verifySRMFolderCreation (String URL, String UserName, String Password) throws InterruptedException, IOException {
		launchBrowser(URL);
		LoginPage LoginPage = new LoginPage(driver);
		LoginPage.loginToTheApplication(UserName, Password);
		SRMCheckList SRMCheckList = new SRMCheckList(driver);
		SRMCheckList.navigateToStudentRecord();
		SRMCheckList.checkVisaCodes();
		SRMCheckList.checkCountryCodes();
		SRMCheckList.checkLanguageCodes();
}
}
 