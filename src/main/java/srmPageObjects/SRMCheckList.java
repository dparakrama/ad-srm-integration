package srmPageObjects;

import basePageObjects.BasicPageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.util.HSSFColor.ROSE;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
Author: fchenh
Created : 24 Jan. 2019
*/
public class SRMCheckList extends BasicPageObjects{
	
	String btnStudents = ".//*[@id='main-nav']/li[2]";
	String btnUpdate = "//*[@id=\"learners\"]/div/div/div/div[5]/table/tbody/tr[1]/td[8]/div/div/a[1]/span";
	
	String btnEnrolments = "//*[@id=\"learnerModal\"]/div/div/div[5]/div[3]/form/ul/li[5]/a";
	String btnVisaCode = "//*[@id=\"learners_visacode\"]/div[1]/span";
	String txtVisaCode = "//*[@id=\"learners_visacode\"]/input[1]";
	String fileVisaCode = "C:\\Users\\fchenh\\Downloads\\SIT SRM testing\\Visa.xlsx";
	
	String btnDemographics = "//*[@id=\"learnerModal\"]/div/div/div[5]/div[3]/form/ul/li[6]/a";
	
	String btnCountry = "//*[@id=\"learners_countryofbirth\"]/div[1]/span/span[2]";
	String txtCountry = "//*[@id=\"learners_countryofbirth\"]/input[1]";
	String fileCountry = "C:\\Users\\fchenh\\Downloads\\SIT SRM testing\\countries.xlsx";
	
	String btnLanguage = "//*[@id=\"learners_studentlote\"]/div[1]/span";
	String txtLanguage = "//*[@id=\"learners_studentlote\"]/input[1]";
	String fileLanguage = "C:\\Users\\fchenh\\Downloads\\SIT SRM testing\\languages.xlsx";

	
	
	
	public SRMCheckList(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void navigateToStudentRecord() throws InterruptedException {
		clickButton(btnStudents);
		Thread.sleep(3000);
		clickButton(btnUpdate);
		Thread.sleep(3000);
	}
	
	public void checkCodes(String TabName, String FieldName, String InputField, String FileName) throws InterruptedException, IOException {
		clickButton(TabName);
		clickButton(FieldName);
		
		FileInputStream file = new FileInputStream(new File(FileName));
		// Get the workbook instance for XLS file
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		// Get first sheet from the workbook
		XSSFSheet sheet = workbook.getSheetAt(0);
		// Iterate through each rows from first sheet
		Iterator<Row> rowIterator = sheet.iterator();
		outerloop: for (int i = 0; i <= sheet.getLastRowNum(); i++) {
			
			//creating formatter
			DataFormatter formatter = new DataFormatter(); 
			Cell cell  = sheet.getRow(i).getCell(0);
			//Returns the formatted value of a cell as a String regardless of the cell type.
			String code = formatter.formatCellValue(cell); 
			
			String codename = sheet.getRow(i).getCell(1).getStringCellValue();
			
			enterText(InputField, code);
			
			// System.out.println("Size is " +
			// driver.findElement(By.xpath("//*[@class='ui-select-choices-row-inner']")).getText().;
			// System.out.println(code);
			
			List<WebElement> allText = driver.findElements(By
					.xpath("//*[@class='ui-select-choices-row-inner']"));
			// gets all the elements found in the drop down list and checks against the excel sheet
			boolean isValid=false; 
			innerloop: for (WebElement element : allText) {
				//System.out.println("Drop down contains " + element.getText());
				String Dropdownval = element.getText();
				if (Dropdownval.contains(codename)){
				//System.out.println("VALUE is " + code + " " + codename);
				clearText(InputField);
				isValid=true;
				break innerloop;
				
			}
			}
			 if(!isValid) {
			System.out.println("DIDN'T FIND " + code +  " " + codename);
			clearText(InputField);
			 }
		}

	}
	
	public void checkVisaCodes() throws InterruptedException, IOException {
		//Method to check visa codes
		checkCodes(btnEnrolments, btnVisaCode, txtVisaCode, fileVisaCode);
		
	}
	
	public void checkCountryCodes() throws InterruptedException, IOException {
		//Method to check country codes
		checkCodes(btnDemographics, btnCountry, txtCountry, fileCountry);
		
	}
	
	public void checkLanguageCodes() throws InterruptedException, IOException {
		//Method to check language codes
		checkCodes(btnDemographics, btnLanguage, txtLanguage, fileLanguage);
		
	}
}
 