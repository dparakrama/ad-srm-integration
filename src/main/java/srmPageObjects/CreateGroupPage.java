package srmPageObjects;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
 * Author: dparakrama Created : Mar 1, 2018
 */
public class CreateGroupPage extends BasicPageObjects {
	
	
	String btnAddGroup = ".//*[@id='sidebar']/div[2]/div[2]/button[1]";
	String txtGroupName = ".//*[@id='group_name_']";
	String btnJurisDiction = ".//*[@id='col-md-3-type-']/button[2]";
	String btnSave = ".//*[@id='groupModal']/div/div/div[6]/button[2]";

	String btnJurisdictionName = "//*[text()[contains(.,'SRM-AUTOMATED-JURISDICTION-DNT')]]";
	String btnSector = ".//*[@id='col-md-3-type-']/button[5]";
	String btnSectorName = "//*[text()[contains(.,'SRM-AUTOMATED-SECTOR-DNT')]]";

	String btnSchool = ".//*[@id='col-md-3-type-']/button[4]";
	String btnSchoolName = "//*[text()[contains(.,'SRM-AUTOMATED-SCHOOL-DNT')]]";

	// ********************************************************************************************
	String btnStudents = ".//*[@id='main-nav']/li[2]";
	String btnCreateStudents = ".//*[@id=\"learners\"]/div/div/div/div[3]/a[1]/button";

	String btnTabDetails = ".//*[@id='learnerModal']/div/div/div[5]/div[3]/form/ul/li[3]/a";
	String txtStudentID = ".//*[@id='learners_localid']";
	String txtFamilyName = ".//*[@id='learners_familyname']";
	String txtGivenName = ".//*[@id='learners_givenname']";
	String txtDOB = ".//*[@id='learners_fsc1']/div[1]/div[2]/div[7]/div/div/div/input"; // 01/02/2008
	String drpSex = "//*[@id='learners_sex']/div[1]/span";
	String btnSex = "//*[text()[contains(.,'Male')]]";
	String txtSchoolId = ".//*[@id='learners_aslschoolid']";
	String drpYearLevel = "//*[@id='learners_yearlevel']/div[1]/span";
	String btn3 = "//*[@id='learners_yearlevel']/input[1]";
	String drpTestLevel = ".//*[@id='learners_testlevel']/div[1]/span";
	String btn32 = ".//*[@id='learners_testlevel']/input[1]";

	String btnTabParticpating = ".//*[@id='learnerModal']/div/div/div[5]/div[3]/form/ul/li[7]/a";
	String drpNumeracyParticipation = ".//*[@id='learners_participationnumeracy']/div[1]/span";
	//String btnParticipating = "//*[text()[contains(.,'Participating')]]";
	String drpReadingParticpation = ".//*[@id='learners_participationreading']/div[1]/span";
	String btnWithdrawn = "//*[text()[contains(.,'Withdrawn')]]";
	String drpWritingParticpation = ".//*[@id='learners_participationwriting']/div[1]/span";
	String btnAbsent = "//*[text()[contains(.,'Absent')]]";

	String btnTabEnrollmentStatus = ".//*[@id='learnerModal']/div/div/div[5]/div[3]/form/ul/li[5]/a";
	String drpFFpos = ".//*[@id='learners_ffpos']/div[1]/span";
	String btnFFPOS = "//*[text()[contains(.,'FFPOS')]]";

	String btnTabDemographics = ".//*[@id='learnerModal']/div/div/div[5]/div[3]/form/ul/li[6]/a";
	String drpIndigenousStatus = ".//*[@id='learners_indigenousstatus']/div[1]/span";
	String btnAboriginal = "//*[text()[contains(.,'Aboriginal but not Torres Strait Islander Origin')]]";
	String drpParent1SchooldEducation = ".//*[@id='learners_parent1schooleducation']/div[1]/span";
	String btnY9 = "//*[text()[contains(.,'Year 9 or equivalent or below')]]";
	String drpParent1NonSchool = ".//*[@id='learners_parent1nonschooleducation']/div[1]/span";
	String btnCertificate = "//*[text()[contains(.,'Certificate I to IV')]]";
	String drpParent1Occupation = ".//*[@id='learners_parent1occupation']/div[1]/span";
	String btnManagement = "//*[text()[contains(.,'Senior management')]]";
	
	String tabDisabilityAdjustment = ".//*[@id='learnerModal']/div/div/div[5]/div[3]/form/ul/li[8]/a";
	String drpNumeracy = ".//*[@id='learners_adjustmentsnumeracy']/div[1]/input";
	String btnSUP = "//*[text()[contains(.,'SUP:')]]";
	String drpCOL = ".//*[@id='learners_adjustmentsconventionsoflanguage']/div[1]/input";
	String btnSCR = "//*[text()[contains(.,'SCR: Scribe')]]";
	String drpReading = ".//*[@id='learners_adjustmentsreading']/div[1]/input";
	String btnOSS = "//*[text()[contains(.,'OSS: Oral sign/support')]]";
	String drpWriting = ".//*[@id='learners_adjustmentswriting']/div[1]";
	String btnETA = "//*[text()[contains(.,' ETA:')]]";

	String btnSaveStudent = ".//*[@id='learnerModal']/div/div/div[6]/button[2]";
	
	String btnDataImportExport = ".//*[@id='main-nav']/li[3]";
	String btnTransfetToADS = "//*[@id='data']/div[1]/button[2]/span";
    String btnScheduleNow = "//*[@id='apiCompiledDataModal']/div/div/div[4]/button[3]/span";
    
    String txtRunGroupName = "//*[@id='data']/div[2]/div[5]/table/tbody/tr/td[4]";
	// **************************************************************************************************

	public CreateGroupPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void createSRMFolderScructure() throws InterruptedException {

		/*
		 * clickButton(btnAddGroup); enterText(txtGroupName,
		 * "SRM-AUTOMATED-JURISDICTION-DNT"); clickButton(btnJurisDiction);
		 * clickButton(btnSave);
		 * 
		 * clickButton(btnJurisdictionName); clickButton(btnAddGroup);
		 * enterText(txtGroupName, "SRM-AUTOMATED-SECTOR-DNT");
		 * clickButton(btnSector); clickButton(btnSave);
		 * 
		 * doubleClickButton(btnJurisdictionName); clickButton(btnSectorName);
		 * clickButton(btnAddGroup); enterText(txtGroupName,
		 * "SRM-AUTOMATED-SCHOOL-DNT"); clickButton(btnSchool);
		 * clickButton(btnSave);
		 */
		Thread.sleep(6000);
		clickButton(btnJurisdictionName);
		clickButton(btnSectorName);
		clickButton(btnSchoolName);
		

	}

	public void createAStudent() throws InterruptedException {
		clickButton(btnStudents);
		clickButton(btnCreateStudents);
		clickButton(btnTabDetails);
		enterText(txtStudentID, "AUTO_STUD");
		enterText(txtFamilyName, "Test");
		enterText(txtGivenName, "Test");
		enterText(txtDOB, "01/02/2008");
		clickButton(drpSex);
		clickButton(btnSex);	
		clickButton(drpYearLevel);
		enterText(btn3, "3\n");
		clickButton(drpTestLevel);
		enterText(btn32, "3\n");
		clickButton(btnTabParticpating);
		clickButton(drpNumeracyParticipation);
		//clickButton(btnParticipating);
		clickButton(drpReadingParticpation);
		clickButton(btnWithdrawn);
		clickButton(drpWritingParticpation);
		clickButton(btnAbsent);

		clickButton(btnTabEnrollmentStatus);
		clickButton(drpFFpos);
		clickButton(btnFFPOS);

		clickButton(btnTabDemographics);
		clickButton(drpIndigenousStatus);
		clickButton(btnAboriginal);
		clickButton(drpParent1SchooldEducation);
		clickButton(btnY9);
		clickButton(drpParent1NonSchool);
		clickButton(btnCertificate);
		clickButton(drpParent1Occupation);
		clickButton(btnManagement);

		clickButton(tabDisabilityAdjustment);
		clickButton(drpNumeracy);
		clickButton(btnSUP);
		clickButton(drpCOL);
		clickButton(btnSCR);

		clickButton(drpReading);
		clickButton(btnOSS);

		clickButton(drpWriting);
		clickButton(btnETA);

		clickButton(btnSaveStudent);

		Thread.sleep(20000);
		
		clickButton(btnDataImportExport);
		clickButton(btnTransfetToADS);
		clickButton(btnScheduleNow);
		
	 getContent(txtRunGroupName, "SRM-AUTOMATED-SCHOOL-DNT");    

		

	}

}
