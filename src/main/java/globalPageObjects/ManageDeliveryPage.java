package globalPageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import basePageObjects.BasicPageObjects;

/**
 * Author: forbeschenh Created : Feb 28, 2018
 */
public class ManageDeliveryPage extends BasicPageObjects {
	String WritingEventName = "SRM integration Writing DNT";
	String WritingTestName = "Writing Regression 2";
	String NumeracyEventName = "SRM integration Numeracy DNT";
	String NumeracyTestName = "Test with survey and intro - MN";
	String RCOLEventName = "SRM integration RCOL DNT";
	String RCOLTestName = "Y5 Reading and COL DF Test";
	String StartDate = "01/01/2000";
	String EndDate = "01/01/2020";

	String btnHamburgerMenu = ".//*[@id=\"top-menu-handle\"]";
	String btnManageDelivery = ".//*[@id=\"top-menu\"]/li[3]/a";

	String btnAssessmentEvents = "//*[@id=\"assessment-events\"]/a";
	String btnSearchHeader = ".//*[@class='search-header']";
	String txtSearchNameFilter = ".//*[@id=\"search-nameFilter\"]";
	String btnPerformSearch = ".//*[@id=\"btnPerformSearch\"]/span[1]";
	String btnActions = ".//*[@id=\"content\"]/div[3]/div/button";
	String btnAddSessionBasedAssessmentEvent = ".//*[@id=\"content\"]/div[3]/div/ul/li[2]/a";
	String txtEventName = ".//*[@id=\"Event_Name\"]";
	String btnSelectTestDropdown = ".//*[@id=\"s2id_TestOrTestGroupDocRef\"]/a/span";
	String txtTestName = ".//*[@id=\"select2-drop\"]/div/input";
	String btnSelectTest = ".//*[@id=\"select2-drop\"]/ul/li/div";
	String btnSaveAssessmentEvent = ".//*[@id=\"content\"]/form/div[2]/span/button/span[1]";
	String btnEditTimingOptions = ".//*[@class='icon-action icon-pencil edit']";
	String txtStartDate = ".//*[@id=\"ae_timingWindowOpen\"]";
	String txtEndDate = ".//*[@id=\"ae_timingWindowClose\"]";
	String btnSaveTiming = ".//*[@id=\"content\"]/div[3]/div[9]/div/section/div[2]/div/span[1]/button/span[1]";
	String btnChangeStatus = ".//*[@class='btn btn-xs button dropdown-toggle btn-primary dropdown-menu-initiated']";
	String btnConfirm = ".//*[@id=\"content\"]/div[3]/div[3]/div/div[2]/div/ul/li[2]/a/span";
	String btnChangeStatusYes = ".//*[@id=\"ng-app\"]/body/div[9]/div[3]/div/button[1]";

	String btnSettings = ".//*[@id=\"top-menu\"]/li[9]/a";
	String btnNaplanSettings = ".//*[@id='naplan-settings']";
	String btnSRMTab = ".//*[@id=\"content\"]/div[6]/div/h2/div/a";
	String btnSelectEventDropdown = ".//*[@id='s2id_autogen2']";
	String txtSRMEventName1 = ".//*[@id=\"s2id_autogen2\"]/ul/li/input";
	String txtSRMEventName2 = ".//*[@id=\"s2id_autogen2\"]/ul/li[2]/input";
	String txtSRMEventName3 = ".//*[@id=\"s2id_autogen2\"]/ul/li[3]/input";
	String btnSelectEvent = ".//*[@id=\"select2-drop\"]/ul/li/div";
	String btnSelectExistingEvent = ".//*[@id=\"s2id_autogen2\"]/ul/li[1]/a";
	String btnSaveSRMSettings = ".//*[@id=\"srmSettings-form\"]/div[14]/div/div/span[1]/button/span[1]";

	String btnResult = ".//*[@id='Grid_1']/table/tbody/tr/td[1]/span/a[1]";
	String btnEdit = ".//*[@id='content']/div[3]/div[1]/div/a";
	String btnDelete = ".//*[@id='content']/div[3]/div/a";
	String btnYesToDelete = "//*[contains(text(), 'Yes')]";

	String btnChange = ".//*[@id='content']/div[3]/div[3]/div/div[2]/div/button";
	String btnCancel = "//*[contains(text(), 'Cancel')]";
	// Yes
	String btnAdminDelete = ".//*[@id='content']/div[3]/div[1]/div/a";
	String btnConfirmDelete = ".//*[@id='ng-app']/body/div[10]/div/div/div[3]/button[1]";
	String txtAdminUN = ".//*[@id='ng-app']/body/div[10]/div/div/div[2]/div[2]/div[2]/div/input";
	String txtAdminPW = ".//*[@id='ng-app']/body/div[10]/div/div/div[2]/div[2]/div[3]/div/input";

	String btnOk = "//*[contains(text(), 'Ok')]";

	public ManageDeliveryPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void navigateManageDelivery() throws InterruptedException {
		clickButton(btnHamburgerMenu);
		clickButton(btnManageDelivery);
	}

	public void navigateSettings() throws InterruptedException {
		clickButton(btnHamburgerMenu);
		clickButton(btnSettings);
		Thread.sleep(2000);
		clickButton(btnNaplanSettings);
		Thread.sleep(2000);
		clickButton(btnSRMTab);
		Thread.sleep(4000);
	}

	public void createSessionBasedAssessmentEvent(String EventName, String TestName, String StartDate, String EndDate)
			throws InterruptedException {

		clickButton(btnActions);
		clickButton(btnAddSessionBasedAssessmentEvent);
		enterText(txtEventName, EventName);
		clickButton(btnSelectTestDropdown);
		enterText(txtTestName, TestName);
		clickButton(btnSelectTest);
		clickButton(btnSaveAssessmentEvent);
		Thread.sleep(2000);
		clickButton(btnEditTimingOptions);
		enterText(txtStartDate, StartDate);
		enterText(txtEndDate, EndDate);
		clickButton(btnSaveTiming);
		clickButton(btnChangeStatus);
		Thread.sleep(4000);
		Thread.sleep(4000);
		clickButton(btnConfirm);
		Thread.sleep(4000);
		clickButton(btnChangeStatusYes);

	}

	
	public void addCreatedEventsToSRMSettings(String XPath, String EventName) throws InterruptedException {

		Thread.sleep(4000);
		clickButton(btnSelectEventDropdown);
		Thread.sleep(2000);
		enterText(XPath, EventName);
		Thread.sleep(2000);
		clickButton(btnSelectEvent);
		Thread.sleep(2000);
		clickButton(btnSaveSRMSettings);
		Thread.sleep(2000);
		clickButton(btnHamburgerMenu);
		Thread.sleep(2000);
		clickButton(btnManageDelivery);

	}

	public void clearExistingEventsInSRMSettings() throws InterruptedException {

		clickButton(btnHamburgerMenu);
		clickButton(btnSettings);
		Thread.sleep(4000);
		clickButton(btnNaplanSettings);
		Thread.sleep(4000);
		clickButton(btnSRMTab);
		Thread.sleep(8000);
		// while there are existing assessment events clear them from the list.
		// Then add the created assessment events
		while (btnSelectExistingEvent != null) {
			try {
				clickButton(btnSelectExistingEvent);

			} catch (Exception e) {
				System.out.println("unable to click");
				addCreatedEventsToSRMSettings(txtSRMEventName1, WritingEventName);
				break;
			}

		}

	}

	public void createWritingSBAE() throws InterruptedException {

		createSessionBasedAssessmentEvent(WritingEventName, WritingTestName, StartDate, EndDate);
		clearExistingEventsInSRMSettings();

	}

	public void createNumeracySBAE() throws InterruptedException {

		createSessionBasedAssessmentEvent(NumeracyEventName, NumeracyTestName, StartDate, EndDate);
		navigateSettings();
		addCreatedEventsToSRMSettings(txtSRMEventName2, NumeracyEventName);

	}

	public void createReadingAndConventionsSBAE() throws InterruptedException {

		createSessionBasedAssessmentEvent(RCOLEventName, RCOLTestName, StartDate, EndDate);
		navigateSettings();
		addCreatedEventsToSRMSettings(txtSRMEventName3, RCOLEventName);

	}

	public void deleteData(String AENAme) throws InterruptedException {
		clickButton(btnAssessmentEvents);
		clickButton(btnSearchHeader);
		Thread.sleep(3000);
		enterText(txtSearchNameFilter, AENAme);
		Thread.sleep(3000);
		clickButton(btnPerformSearch);
		Thread.sleep(3000);
		clickButton(btnResult);
		Thread.sleep(3000);

		// Canceling the assignment event and the admin delete it
		clickButton(btnChange);
		Thread.sleep(3000);
		clickButton(btnCancel);
		Thread.sleep(6000);
		clickButton(btnYesToDelete);
		Thread.sleep(15000);
		clickButton(btnAdminDelete);
		Thread.sleep(30000);
		clickButton(btnConfirmDelete);
		Thread.sleep(6000);
		enterText(txtAdminUN, "sanjeevk");
		enterText(txtAdminPW, "Password1!");
		clickButton(btnOk);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(btnOk)));
		clickButton(btnOk);
		
		Thread.sleep(10000);
	}

	public void DeleteAssessmentEvents() throws InterruptedException {
		navigateManageDelivery();
		deleteData(WritingEventName);
		deleteData(NumeracyEventName);
		deleteData(RCOLEventName);
	}

}
