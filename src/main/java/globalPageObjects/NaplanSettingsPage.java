package globalPageObjects;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Feb 28, 2018
 */
public class NaplanSettingsPage extends BasicPageObjects{

	String btnHamburgerMenu = ".//*[@id='top-menu-handle']";
	String btnSettings = ".//*[@id='top-menu']/li[9]/a";
	String btnNaplanSettings = ".//*[@id='naplan-settings']";
    String btnSRMTab = ".//*[@id='content']/div[6]/div/h2";
	String btnTestSRMConnection = ".//*[@id='srmSettings-form']/div[13]/a";
	String btnConnectionSuccessMessage = ".//*[@class='callout ng-scope callout-success']/span";
	
	public NaplanSettingsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void navigateToNaplanSettings () throws InterruptedException {	
		clickButton(btnHamburgerMenu);
		clickButton(btnSettings);
		clickButton(btnNaplanSettings);
		
	}
	
	public void verifySRMConnection () throws InterruptedException {
		
		clickButton(btnSRMTab);
		clickButton(btnTestSRMConnection);
		Thread.sleep(3000);
		getContent( btnConnectionSuccessMessage, "Test SRM connection was successful.");
		
		
	}
	
	
	
}

