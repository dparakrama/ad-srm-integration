package globalPageObjects;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Feb 28, 2018
 */
public class AuthorPage extends BasicPageObjects {

	String btnHamburgerMenu = ".//*[@id='top-menu-handle']";
	String btnAuthor = ".//*[@id='top-menu']/li[2]/a";
	String btnDisiplines = ".//*[@id='disciplines']/a";
	String btnSearchHeader = ".//*[@class='search-header']";
	String txtSerchnameFilter = ".//*[@id='search-nameFilter']";
	String btnPerformSearch = ".//*[@id='btnPerformSearch']";
	String txtResult = ".//*[@id='Grid_1']/table/tbody/tr/td[1]/span/a[1]";
	
	
	public AuthorPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	public void navigateToAuthor() throws InterruptedException {
		clickButton(btnHamburgerMenu);
		clickButton(btnAuthor);
	}
	
	public void verifyDisiplinesReading() throws InterruptedException {
		clickButton(btnDisiplines);
		clickButton(btnSearchHeader);
		enterText(txtSerchnameFilter, "Reading");
		clickButton(btnPerformSearch);
		getContent(txtResult, "Reading");
		
	}
	
	public void verifyDisiplinesWriting() throws InterruptedException {
		clickButton(btnDisiplines);
		clickButton(btnSearchHeader);
		enterText(txtSerchnameFilter, "Writing");
		clickButton(btnPerformSearch);
		getContent(txtResult, "Writing");
		
	}
	
	
	public void verifyDisiplinesNumeracy() throws InterruptedException {
		clickButton(btnDisiplines);
		clickButton(btnSearchHeader);
		enterText(txtSerchnameFilter, "Numeracy");
		clickButton(btnPerformSearch);
		getContent(txtResult, "Numeracy");
		
	}
	
	
	public void verifyConventionsofLanguage() throws InterruptedException {
		clickButton(btnDisiplines);
		clickButton(btnSearchHeader);
		enterText(txtSerchnameFilter, "Conventions of Language");
		clickButton(btnPerformSearch);
		getContent(txtResult, "Conventions of Language");
		
	}
	
	
	
	
}

