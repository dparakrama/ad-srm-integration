package globalPageObjects;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Feb 28, 2018
 */
public class ManagePeoplePage extends BasicPageObjects{

	String btnHamburgerMenu = ".//*[@id='top-menu-handle']";
    String btnManagePeople = ".//*[@id='top-menu']/li[5]/a";
	String btnDisabilityAdjustmentCodes = ".//*[@id='personal-needs-and-preferences']/a";
	String tblDisabilityAdjustmentCodes = ".//*[@id='Grid_1']/table";
	String tbleBefore = ".//*[@id='Grid_1']/table/tbody/tr[";
	String tblAfter = "]/td[1]";
	
	
	public ManagePeoplePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	public void navigateToManagePeople () throws InterruptedException {
		clickButton(btnHamburgerMenu);
		clickButton(btnManagePeople);	
	}
	
	public void verifyDisabilityAdjustmentCodes() throws InterruptedException {		
		clickButton(btnDisabilityAdjustmentCodes);
		findTableElement(tblDisabilityAdjustmentCodes, "AAM", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "AIA", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "AIM", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "AIV", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "ALL", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "AST", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "AVM", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "COL", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "ETA", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "ETB", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "ETC", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "ETD", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "OFF", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "OSS", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "RBK", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "SCR", tbleBefore, tblAfter);
		findTableElement(tblDisabilityAdjustmentCodes, "SUP", tbleBefore, tblAfter);
		
	}
	
	
	
}

