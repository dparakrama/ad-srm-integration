package localPageObjects;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
 * Author: dparakrama Created : Mar 14, 2018
 */
public class ManageDeliveryPage extends BasicPageObjects {

	String btnHamburgerMenu = ".//*[@id='top-menu-handle']";
	String btnManageDelivery = "//*[@id='top-menu']/li[5]/a";
	String btnSearchAssessmentEvent = "//*[@id='content']/div[6]/form/h3";
	String txtSearchCriteria = "//*[@id='search-nameFilter']";
	String btnSearch = "//*[@id='content']/div[6]/form/div[2]/div[2]/div";

	String btnResult = "//*[@id='Grid_1']/table/tbody/tr/td[1]/span/a[1]";
	String btnUserEnrollments = "//*[@id='content']/div[3]/div[11]/h2/span";
	String drpYearLevel = "//*[@id='s2id_userEnrolmentGroupsSelect']/ul";
	String btnYear3 = "//*[@id='select2-drop']/ul/li[1]/div";

	String btnSave = "//*[@id='ctrl']/div[6]/div/span[1]/button/span[1]";

	public ManageDeliveryPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void setUserYearLevel(String AssessmentName)
			throws InterruptedException {
		clickButton(btnHamburgerMenu);
		clickButton(btnManageDelivery);
		clickButton(btnSearchAssessmentEvent);
		enterText(txtSearchCriteria, AssessmentName);
		clickButton(btnSearch);
		clickButton(btnResult);
		clickButton(btnUserEnrollments);
		clickButton(drpYearLevel);
		clickButton(btnYear3);
		clickButton(btnSave);
	}

	public void setWritingEvent() throws InterruptedException {

		setUserYearLevel("SRM integration Writing DNT");
	}

	public void setNumeracyEvent() throws InterruptedException {

		setUserYearLevel("SRM integration Numeracy DNT");
	}

	public void setRCOLEventEvent() throws InterruptedException {

		setUserYearLevel("SRM integration RCOL DNT");
	}
}