package localPageObjects;

import org.openqa.selenium.WebDriver;

import basePageObjects.BasicPageObjects;

/**
Author: dparakrama
Created : Feb 28, 2018
 */
  public class ManagePeoplePage extends BasicPageObjects {

		String btnHamburgerMenu = ".//*[@id='top-menu-handle']";
	    String btnManagePeople = ".//*[@id='top-menu']/li[7]/a";
        String btnJurisdiction= ".//*[@id='organisations']";
	    String btnAddJurisdiction = ".//*[@class='button btn add btn-primary']";
       
	    String txtName = ".//*[@id='Name']";
	    String txtSRMIndetifier = "//*[@id='Taxonomy_Docs_0__Fields_1__StrValue']";
        String btnCrate = ".//*[@id='content']/form/div[4]/span/button/span[1]";
	
        String btnSchools = ".//*[@id='organisation-units']/a";
        String btnAddSchool = ".//*[@class='button btn add btn-primary']";
        
        String drpJurisdicton = ".//*[@id='s2id_OrgId']/a/span";
        String txtDrpInput = ".//*[@id='select2-drop']/div/input";
        String btnResult = ".//*[@id='select2-drop']/ul/li/div";
        
        String btnSearchBar = "//*[contains(text(), 'Search for')]";
        String txtSearchNameFilter = ".//*[@id='search-nameFilter']";
	    String btnSearch = ".//*[@id='btnPerformSearch']";
	    String btnResults = ".//*[@id='Grid_1']/table/tbody/tr[1]/td[4]/span/a[1]";
	    
	    String btnEdit = ".//*[@id='content']/div[3]/div/a";
	    String btnYesToDelete = "//*[contains(text(), 'Yes')]";
	    String btnResult2 = ".//*[@id='Grid_1']/table/tbody/tr/td[2]/span/a[1]";

	    
	public ManagePeoplePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	public void navigateToManagePeople () throws InterruptedException {	
		clickButton(btnHamburgerMenu);
		clickButton(btnManagePeople);
		
	}
	
	public void createAJurisdiction() throws InterruptedException {
		clickButton(btnJurisdiction);
		clickButton(btnAddJurisdiction);
		enterText(txtName, "SRM-AUTOMATED-JURISDICTION-DNT");
		Thread.sleep(3000);
		enterText(txtSRMIndetifier, "SRM-AUTOMATED-JURISDICTION-DNT");
		clickButton(btnCrate);
		Thread.sleep(8000);
	}
	
	public void createASchool() throws InterruptedException {
		clickButton(btnSchools);
		clickButton(btnAddSchool);
		clickButton(drpJurisdicton);
		enterText(txtDrpInput, "SRM-AUTOMATED-JURISDICTION-DNT");
		//identifier srm-automated-dnt
		clickButton(btnResult);
		enterText(txtName, "SRM-AUTO");
		//identifier srm-automated-school-dnt
		Thread.sleep(3000);
		clickButton("//*[@id='content']/form/div[2]/span/button/span[1]");
		Thread.sleep(3000);
		
	}
	
	
	public void DeleteData () throws InterruptedException {
		
		navigateToManagePeople ();
		clickButton(btnSchools);
		
		clickButton(btnSearchBar);
		enterText(txtSearchNameFilter, "SRM-AUTO");
		Thread.sleep(3000);
		clickButton(btnSearch);
		Thread.sleep(3000);
		clickButton(btnResults);
		Thread.sleep(3000);
		clickButton(btnEdit);
		Thread.sleep(3000);
		clickButton(btnEdit);
		Thread.sleep(3000);
		clickButton(btnYesToDelete);
		
		clickButtonbyCss(btnJurisdiction);
		clickButton(btnSearchBar);
		enterText(txtSearchNameFilter, "SRM-AUTOMATED-JURISDICTION-DNT");
		Thread.sleep(3000);
		clickButton(btnSearch);
		Thread.sleep(3000);
		clickButton(btnResult2);
		clickButton(btnEdit);
		Thread.sleep(3000);
		clickButton(btnEdit);
		Thread.sleep(3000);
		clickButton(btnYesToDelete);
		
		
	}
}

